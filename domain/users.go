package domain

import (
	"encoding/json"
	"net/http"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.com/ghati/lastfapm/dao"
	"gitlab.com/ghati/lastfapm/models"
)

type CreateUserRequest struct {
	Username string
	Password string
}

// CreateUserHandler creates a new user
func CreateUserHandler(w http.ResponseWriter, r *http.Request) {

	// Parse the incoming CreateUserRequest
	var createUserRequest CreateUserRequest
	err := json.NewDecoder(r.Body).Decode(&createUserRequest)
	if err != nil {
		ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
			Error:       "Unable to parse request!",
			Description: err.Error(),
		})
		return
	}

	// Check if user exists
	var userExists int64
	dao.Db.Where("username = ?", createUserRequest.Username).Find(&models.User{}).Count(&userExists)
	if userExists != 0 {
		log.Info().Msgf("userExists=%v", userExists)
		ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
			Error: "Username already exists!",
		})
		return
	}

	// Hash the password...
	hashedPassword, err := HashPassword(createUserRequest.Password)
	if err != nil {
		log.Error().Err(err).Msg("Got an error when generating password hash")
		ReturnJSON(w, http.StatusInternalServerError, models.HttpErrorResponse{
			Error: "Error Occurred :(",
		})
		return
	}

	// Create the User
	var user models.User
	user.ID = uuid.New().String()
	user.Username = createUserRequest.Username
	user.HashedPassword = hashedPassword

	// And commit to DB
	dao.Db.Create(&user)

	log.Info().Str("username", createUserRequest.Username).Msg("Created new User!")

	ReturnJSON(w, http.StatusCreated, models.HttpResponse{
		Message: "Created User",
		Entity:  user,
	})
	return

}

func GetUserHandler(w http.ResponseWriter, r *http.Request) {

	userSession, _ := GetUserSessionFromRequest(r)
	user, _ := dao.GetUserById(userSession.UserID)

	log.Info().Str("username", user.Username).Msg("Returning GetUserHandler")

	ReturnJSON(w, http.StatusOK, user)

	return
}

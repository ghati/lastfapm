package domain

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/spf13/viper"
)

func init() {
	SetupConfig("../", "")
}

func TestVersionHandler(t *testing.T) {

	req, err := http.NewRequest("GET", "/version", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	server := http.HandlerFunc(VersionHandler)
	server.ServeHTTP(rr, req)

	// The status code must be 200
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got=%v want=%v",
			status, http.StatusOK)
	}

	// Cast the response body to a Version
	var responseVersion Version
	json.Unmarshal([]byte(rr.Body.String()), &responseVersion)

	// Assert the version matches
	if responseVersion.Name != viper.GetString("server.name") {
		t.Errorf("handler returned unexpected body: got=%v want=%v",
			responseVersion.Name, viper.GetString("server.name"))
	}
}

package domain

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.com/ghati/lastfapm/dao"
	"gitlab.com/ghati/lastfapm/models"
)

type AddFapRequest struct {
	FapType    string            `json:"fap_type"`
	FapContent models.FapContent `json:"fap_content"`
}

func AddFapsHandler(w http.ResponseWriter, r *http.Request) {

	userSession, _ := GetUserSessionFromRequest(r)
	user, _ := dao.GetUserById(userSession.UserID)

	var addFapRequest AddFapRequest
	err := json.NewDecoder(r.Body).Decode(&addFapRequest)
	if err != nil {
		ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
			Error:       "Unable to parse request!",
			Description: err.Error(),
		})
		return
	}

	var fapToPersist models.Fap

	fapToPersist.FapID = uuid.New().String()
	fapToPersist.FapperID = user.ID

	log.Info().Str("fap_type", addFapRequest.FapType).Str("username", user.Username).Msg("Adding new fap")

	switch addFapRequest.FapType {
	case "repost":

		var existingFapContent models.FapContent
		dao.Db.Find(&existingFapContent, "content_id = ?", addFapRequest.FapContent.ContentID)

		if existingFapContent.ContentID == "" {
			ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
				Error:       "Bad Request",
				Description: "fap_content with that content_id does not exist!",
			})
			return
		}

		fapToPersist.ContentID = existingFapContent.ContentID
		fapToPersist.Content = existingFapContent

	case "new":
		var newFapContent models.FapContent
		newFapContent.ContentID = uuid.New().String()
		newFapContent.ContentCreator = user.ID
		newFapContent.Title = addFapRequest.FapContent.Title

		dao.Db.Create(&newFapContent)

		fapToPersist.ContentID = newFapContent.ContentID
		fapToPersist.Content = newFapContent

	default:
		ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
			Error:       "Bad Request",
			Description: "Invalid fap_type",
		})
		return
	}

	dao.Db.Create(&fapToPersist)

	ReturnJSON(w, http.StatusCreated, models.HttpResponse{
		Message: "Added Fap",
		Entity:  &fapToPersist,
	})
}

type GetFapsResponse struct {
	User models.User  `json:"user"`
	Faps []models.Fap `json:"faps"`
}

func GetFapsByUserHandler(w http.ResponseWriter, r *http.Request) {

	username := r.FormValue("username")
	log.Info().Str("username", username).Msg("GetFapsByUserHandler")

	if username == "" {
		ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
			Error: "No username specified",
		})
		return
	}

	user, err := dao.GetUserByUsername(username)
	if err != nil {
		ReturnJSON(w, http.StatusNotFound, models.HttpErrorResponse{
			Error: "Username not found",
		})
		return
	}

	var faps []models.Fap
	dao.Db.Order("created_at desc").Find(&faps, "fapper_id = ?", user.ID)

	var toReturn GetFapsResponse
	toReturn.User = user

	for _, fap := range faps {
		var fapContent models.FapContent
		dao.Db.First(&fapContent, "content_creator = ?", fap.FapperID)
		fap.Content = fapContent
		toReturn.Faps = append(toReturn.Faps, fap)
	}

	ReturnJSON(w, http.StatusCreated, toReturn)
	return
}

type LatestFap struct {
	FapID     string            `json:"fap_id"`
	Username  string            `json:"username"`
	CreatedAt time.Time         `json:"created_at"`
	Content   models.FapContent `json:"content"`
}

func GetLatestFaps(w http.ResponseWriter, r *http.Request) {

	log.Info().Msg("GetLatestFaps")

	var toReturn []LatestFap

	rows, err := dao.Db.Table("faps").Select("fap_id, faps.created_at, username, content_id").Joins("join users on faps.fapper_id = users.id").Order("faps.created_at desc").Limit(5).Rows()

	if err != nil {
		ReturnJSON(w, 500, models.HttpErrorResponse{
			Error:       "Internal Server Error",
			Description: err.Error(),
		})
	}

	for rows.Next() {

		var fapID string
		var createdAt time.Time
		var username string
		var contentId string

		rows.Scan(&fapID, &createdAt, &username, &contentId)

		var lastestFap LatestFap
		lastestFap.FapID = fapID
		lastestFap.CreatedAt = createdAt
		lastestFap.Username = username

		dao.Db.First(&lastestFap.Content, "content_id = ?", contentId)

		toReturn = append(toReturn, lastestFap)
	}

	ReturnJSON(w, http.StatusCreated, toReturn)
	return
}

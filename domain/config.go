package domain

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

// SetupConfig -  Setup the application config by reading the config file via Viper
func SetupConfig(configPath string, serviceVersion string) {
	viper.SetConfigName("config")
	viper.AddConfigPath(configPath)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("Error when reading the config!")
	}

	if serviceVersion == "" {
		serviceVersion = "undefined"
	}

	viper.Set("server.version", serviceVersion)
}

package domain

import (
	"encoding/json"
	"net/http"
)

// ReturnJSON is a utility to return a JSON for a HTTP request
func ReturnJSON(w http.ResponseWriter, statusCode int, modelToReturn interface{}) {

	// Set the header to JSON
	w.Header().Set("Content-Type", "application/json")

	// Set the status code
	w.WriteHeader(statusCode)

	// Encode the model to a JSON
	json.NewEncoder(w).Encode(modelToReturn)

	return
}

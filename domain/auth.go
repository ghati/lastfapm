package domain

import (
	"encoding/base64"
	"encoding/gob"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"github.com/gorilla/sessions"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/ghati/lastfapm/dao"
	"gitlab.com/ghati/lastfapm/models"
	"golang.org/x/crypto/bcrypt"
)

const hashingCost int = 10

type DecodedAuthorization struct {
	Username string
	Password string
}

type UserLoginRequest struct {
	Username string
	Password string
}

type UserSession struct {
	UserID        string
	Authenticated bool
}

// store will hold all session data
var store *sessions.CookieStore

func InitCookieStore() {

	store = sessions.NewCookieStore(
		[]byte(viper.GetString("cookies.auth_key")),
		[]byte(viper.GetString("cookies.encryption_key")),
	)

	if viper.GetBool("cookies.secure") {
		log.Info().Msg("cookies.secure was set to true - using SameSiteNoneMode")
		store.Options.SameSite = http.SameSiteNoneMode
		store.Options.Secure = true
	}

	gob.Register(UserSession{})
}

// AuthenticationMiddleware enforces that the User is authenticated
func AuthenticationMiddleware() Middleware {

	return func(f http.HandlerFunc) http.HandlerFunc {

		return func(w http.ResponseWriter, r *http.Request) {

			_, err := GetUserSessionFromRequest(r)
			if err != nil {
				ReturnJSON(w, http.StatusUnauthorized, models.HttpErrorResponse{
					Error:       "Bad or Invalid Creds",
					Description: err.Error(),
				})
				return
			}

			f(w, r)

		}
	}
}

func GetUserSessionFromRequest(r *http.Request) (UserSession, error) {

	var userSession UserSession

	// Get the session based on the cookie
	session, err := store.Get(r, "lfpm-session")
	if err != nil {
		return userSession, err
	}

	// Get user from session values
	val := session.Values["user"]
	userSession, ok := val.(UserSession)
	if !ok {
		return userSession, errors.New("Session not found in request")
	}

	if userSession.UserID == "" {
		return userSession, errors.New("Session not found in request")
	}

	if userSession.Authenticated == false {
		return userSession, errors.New("Session was not authenticated")
	}

	return userSession, nil
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {

	// Get or create session from request
	session, err := store.Get(r, "lfpm-session")
	if err != nil {
		log.Error().Err(err).Msg("Returning error when getting cookie from store")
		ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
			Error:       "Invalid Session",
			Description: "Invalid Session",
		})
		return
	}

	var decodedAuthorization DecodedAuthorization
	err = json.NewDecoder(r.Body).Decode(&decodedAuthorization)
	if err != nil {
		ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
			Error:       "Unable to parse request!",
			Description: err.Error(),
		})
		return
	}

	user, err := dao.GetUserByUsername(decodedAuthorization.Username)
	if err != nil {
		ReturnJSON(w, http.StatusUnauthorized, models.HttpErrorResponse{
			Error:       "Bad or Invalid Creds",
			Description: "User does not exists",
		})
		return
	}

	if !CheckPasswordHash(decodedAuthorization.Password, user.HashedPassword) {
		ReturnJSON(w, http.StatusUnauthorized, models.HttpErrorResponse{
			Error:       "Bad or Invalid Creds",
			Description: "Incorrect password",
		})
		return
	}

	// Generate the user session
	var userSession UserSession
	userSession.UserID = user.ID
	userSession.Authenticated = true

	// Save the Session
	session.Values["user"] = userSession
	err = session.Save(r, w)
	if err != nil {
		log.Error().Err(err).Msg("Returning error when saving cookie to store")
		ReturnJSON(w, http.StatusBadRequest, models.HttpErrorResponse{
			Error:       "Invalid Session",
			Description: err.Error(),
		})
		return
	}

	log.Info().Str("username", user.Username).Msg("Saved cookie!")

	ReturnJSON(w, http.StatusAccepted, models.HttpResponse{
		Message: "Logged in",
		Entity:  user,
	})
	return
}

// HashPassword generates a bycrpt hash from the passed password
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), hashingCost)
	return string(bytes), err
}

// CheckPasswordHash compares the passed hash and password
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// GetDecodedCredsFromAuthorizationHeader returns the decoded credentials supplied via the Authorization header
func GetDecodedCredsFromAuthorizationHeader(authorizationHeader string) (DecodedAuthorization, error) {

	var decodedAuthorization DecodedAuthorization

	if authorizationHeader == "" {
		return decodedAuthorization, errors.New("No Authorization Header was provided in the HTTP headers")
	}

	basicAuthHeaderValue := strings.Split(authorizationHeader, "Basic ")
	if !(len(basicAuthHeaderValue) >= 2) {
		return decodedAuthorization, errors.New("Invalid Authorization Header")
	}

	decodedCreds, _ := base64.StdEncoding.DecodeString(basicAuthHeaderValue[1])

	splitDecodedCreds := strings.Split(string(decodedCreds), ":")
	if !(len(splitDecodedCreds) == 2) {
		return decodedAuthorization, errors.New("Invalid Authorization Header")
	}

	decodedAuthorization.Username = splitDecodedCreds[0]
	decodedAuthorization.Password = splitDecodedCreds[1]

	return decodedAuthorization, nil
}

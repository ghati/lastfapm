package models

import "time"

type Fap struct {
	FapID     string     `gorm:"primaryKey;unique;not null" json:"fap_id"`
	CreatedAt time.Time  `gorm:"not null" json:"created_at"`
	FapperID  string     `gorm:"not null" json:"fapper_id"`
	Content   FapContent `gorm:"foreignKey:ContentID" json:"content"`
	ContentID string     `gorm:"not null" json:"content_id"`
}

type FapContent struct {
	ContentID      string    `gorm:"primaryKey;unique;not null" json:"content_id"`
	CreatedAt      time.Time `gorm:"not null" json:"created_at"`
	ContentCreator string    `gorm:"not null" json:"-"`
	Title          string    `gorm:"not null" json:"title"`
}

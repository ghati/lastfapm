package models

import (
	"time"
)

type User struct {
	ID             string    `gorm:"primaryKey;unique;not null" json:"id"`
	CreatedAt      time.Time `json:"created_at"`
	Username       string    `gorm:"primaryKey;unique;not null" json:"username"`
	HashedPassword string    `gorm:"not null" json:"-"`
}

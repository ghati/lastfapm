package models

type HttpResponse struct {
	Message string      `json:"message"`
	Entity  interface{} `json:"entity,omitempty"`
}

type HttpErrorResponse struct {
	Error       string `json:"error_message"`
	Description string `json:"error_description,omitempty"`
}

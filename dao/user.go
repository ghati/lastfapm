package dao

import (
	"errors"

	"gitlab.com/ghati/lastfapm/models"
)

func GetUserByUsername(username string) (models.User, error) {
	var user models.User
	Db.First(&user, "username = ?", username)

	if user.ID == "" {
		return user, errors.New("User not found")
	}

	return user, nil
}

func GetUserById(id string) (models.User, error) {
	var user models.User
	Db.First(&user, "id = ?", id)

	if user.ID == "" {
		return user, errors.New("User not found")
	}

	return user, nil
}

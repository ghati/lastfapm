package dao

import (
	"log"
	"os"

	zl "github.com/rs/zerolog/log"
	"gitlab.com/ghati/lastfapm/models"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// Db is the Shared Database instance
var Db *gorm.DB

// InitDatabase initializes the database
func InitDatabase() {

	ormLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent, // Log level
		},
	)

	// dsn := "user=postgres password=postgres dbname=lastfapm port=5432 sslmode=disable"
	// db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
	// 	Logger: ormLogger,
	// })

	db, err := gorm.Open(sqlite.Open("lastfapm.db"), &gorm.Config{
		Logger: ormLogger,
	})

	if err != nil {
		zl.Fatal().Err(err).Msg("Failed to connect to database!")
	}

	Db = db

	Db.AutoMigrate(&models.User{})
	Db.AutoMigrate(&models.Fap{})
	Db.AutoMigrate(&models.FapContent{})
}

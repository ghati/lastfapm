#!/bin/bash

PID=$(ps -eaf | grep lastfapm | grep -v grep | awk '{print $2}')

if [[ "" != "$PID" ]]; then
    echo "killing $PID"
    kill -9 $PID
fi
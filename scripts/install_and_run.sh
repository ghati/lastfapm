#!/bin/bash

set -e

echo "We out here in $(pwd)"

echo "==> Killing the old Musick.."
./kill.sh

echo "==> Deleting the old binary..."
rm -rf ../service/lastfapm

mkdir -p ../service
mv ../lastfapm ../service

cd ../service

echo "==> Starting the new Service!"
./lastfapm > service.log 2>&1 & 
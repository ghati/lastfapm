package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/ghati/lastfapm/dao"
	"gitlab.com/ghati/lastfapm/domain"
)

var (
	serviceVersion string
)

func init() {
	domain.SetupLogging()
	domain.SetupConfig(".", serviceVersion)
	dao.InitDatabase()
	domain.InitCookieStore()
}

func main() {

	port := viper.GetString("server.port")
	baseURL := viper.GetString("server.base_url")

	router := mux.NewRouter()
	router.HandleFunc("/", domain.VersionHandler).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc(fmt.Sprintf("/%s", baseURL), domain.VersionHandler).Methods(http.MethodGet, http.MethodOptions)

	router.HandleFunc(fmt.Sprintf("/%s/login", baseURL), domain.LoginHandler).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc(fmt.Sprintf("/%s/users", baseURL), domain.CreateUserHandler).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc(fmt.Sprintf("/%s/users", baseURL),
		domain.Chain(domain.GetUserHandler, domain.AuthenticationMiddleware()),
	).Methods(http.MethodGet, http.MethodOptions)

	router.HandleFunc(fmt.Sprintf("/%s/faps", baseURL),
		domain.Chain(domain.AddFapsHandler, domain.AuthenticationMiddleware()),
	).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc(fmt.Sprintf("/%s/faps", baseURL),
		domain.Chain(domain.GetFapsByUserHandler),
	).Methods(http.MethodGet, http.MethodOptions)

	router.HandleFunc(fmt.Sprintf("/%s/faps/all", baseURL),
		domain.Chain(domain.GetLatestFaps),
	).Methods(http.MethodGet, http.MethodOptions)

	router.Use(domain.CorsMiddleware)
	router.Use(mux.CORSMethodMiddleware(router))

	log.Info().Str("port", port).Str("baseURL", baseURL).Msg("Starting HTTP Server!")

	http.ListenAndServe(":"+port, handlers.CompressHandler(router))
}
